---
description: Table of the testing status of the various modules.
---

# Testing Status

| Priority |                         Module                        | Plugins |    Intel    | Apple Silicon |
| :------: | :---------------------------------------------------: | :-----: | :---------: | :-----------: |
|     1    |             [afsocket](modules/afsocket/)             |    17   |    Tested   |     Tested    |
|     1    |               [affile](modules/affile/)               |    6    |    Tested   |     Tested    |
|     1    |              [afprog](modules/afprog-2/)              |    2    |    Tested   |     Tested    |
|     1    |       [system-source](modules/system-source-1/)       |    1    | Tested \[F] |  Tested \[F]  |
|     1    |             [afuser](modules/afuser-1.md)             |    1    |    Tested   |     Tested    |
|     1    |         [pseudofile](modules/pseudofile-1.md)         |    1    |    Tested   |     Tested    |
|     2    |          [mod-python](modules/mod-python-7/)          |    7    |    Tested   |     Tested    |
|     2    |          [afmongodb](modules/afmongodb-1.md)          |    1    |    Tested   |     Tested    |
|     2    |               [http](modules/http-1.md)               |    1    |    Tested   |     Tested    |
|     2    |            [riemann](modules/riemann-1.md)            |    1    |    Tested   |     Tested    |
|     2    |              [redis](modules/redis-1.md)              |    1    |    Tested   |     Tested    |
|     2    | [elasticsearch-http](modules/elasticsearch-http-1.md) |    1    |    Tested   |     Tested    |
|     2    |              [afsql](modules/afsql-1.md)              |    1    | Tested \[F] |  Tested \[F]  |
|     2    |             [afsmtp](modules/afsmtp-1.md)             |    1    | Tested \[F] |  Tested \[F]  |

