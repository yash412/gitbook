---
description: >-
  The afsocket module provides socket based transports for syslog-ng, such as
  the udp(), tcp() and syslog() drivers. This module is compiled with SSL
  support.
---

# afsocket \[17]

This module contains the following plugins:

|                                Plugin                               |    Status    |
| :-----------------------------------------------------------------: | :----------: |
|           [Network ( Source )](network-source-driver.md)            |    Tested    |
|      [Network ( Destination )](network-destination-driver.md)       |    Tested    |
|      [Syslog ( Source )](syslog-source-destination-driver.md)       |    Tested    |
|    [Syslog ( Destination )](syslog-source-destination-driver.md)    |    Tested    |
|                  [TLS Encryption](tls-encryption/)                  |    Tested    |
|         [unix-stream (Source)](unix-stream-source-driver.md)        |    Tested    |
|    [unix-stream (Destination)](unix-stream-destination-driver.md)   |    Tested    |
|    [unix-dgram (Source)](unix-dgram-source-destination-driver.md)   |    Tested    |
| [unix-dgram (Destination)](unix-dgram-source-destination-driver.md) |    Tested    |
|                      Systemd-syslog ( Source )                      | Incompatible |
|                       TCP (Source/Destination)                      |   Obsolete   |
|                      TCP6 (Source/Destination)                      |   Obsolete   |
|                       UDP (Source/Destination)                      |   Obsolete   |
|                      UDP6 (Source/Destination)                      |   Obsolete   |
