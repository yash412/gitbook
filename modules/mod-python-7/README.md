---
description: >-
  The python module provides Python scripting support for syslog-ng. It works
  with python3.
---

# mod-python \[7]

### Plugins

|                            Plugin                            |  Status  |
| :----------------------------------------------------------: | :------: |
|         [python ( Source )](python-source-driver.md)         |  Tested  |
|    [python ( Destination )](python-destination-driver.md)    |  Tested  |
| [python-fetcher ( Source )](python-fetcher-source-driver.md) |  Tested  |
|          python\_http\_header ( Inner Destination )          | Untested |
|                       python ( Parser )                      | Untested |
|                 python ( Template Function )                 | Untested |
|                       python ( Root )                        | Untested |

