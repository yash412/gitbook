---
description: The affile module provides file source & destination support for syslog-ng.
---

# affile \[6]

### Plugins

|                            Plugin                            | Status |
| :----------------------------------------------------------: | :----: |
|           [File ( Source )](file-source-driver.md)           | Tested |
|      [File ( Destination )](file-destination-driver.md)      | Tested |
|           [Pipe ( Source )](pipe-source-driver.md)           | Tested |
|      [Pipe ( Destination )](pipe-destination-driver.md)      | Tested |
| [Wildcard\_file ( Source )](wildcard\_file-source-driver.md) | Tested |
|          [Stdin ( Source )](stdin-source-driver.md)          | Tested |
