---
description: The afprog module provides program source & destination drivers for syslog-ng.
---

# afprog \[2]



### Plugins

|                         Plugin                         | Status |
| :----------------------------------------------------: | :----: |
|      [program (Source)](program-source-driver.md)      | Tested |
| [program (Destination)](program-destination-driver.md) | Tested |
