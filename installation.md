---
description: >-
  How to compile syslog-ng on macOS. This guide includes specific instructions
  for macOS with Apple Silicon.
---

# Installation

At present, macOS syslog-ng is not supported on the official repository. However, you can compile it from the source using this guide.&#x20;

_Note:_ the guide is tested on ARM macOS Ventura 13.4, and Intel macOS Monterey 12.6.6 machines, your actual system may require additional steps or slightly different settings.

## Compiling from source

Like every project syslog-ng also uses different libraries and build-systems that must be installed for compiling and running properly. These dependencies can be satisfied by compiling every-each libs and tools manually, but it might be preferred to do it the easy way. [Homebrew](http://brew.sh/) is a package manager for macOS that has great community and support. You can also use it to install the dependencies you need.

### Dependencies

1. [Install Homebrew](https://docs.brew.sh/Installation) on your system.

{% hint style="info" %}
* Don't forget to add homebrew to your path, follow the instructions in your terminal!
* This will install Command Line Tools for Xcode as well if not already prresented on the system that would also be required anyway for a seamless syslog-ng build.
{% endhint %}

2. Perform [`brew update`](#user-content-fn-1)[^1] if you have not done it yet.
3.  Install the following dependencies:

    * automake
    * autoconf
    * autoconf-archive
    * binutils
    * bison
    * flex
    * gcc@11
    * glib
    * ivykis
    * json-c
    * libtool
    * pcre
    * pkg-config
    * openssl

    The following package(s) might be needed tas well depending on your macOS version and architecture

    * net-snmp

    Extra modules might require the following

    * hiredis
    * ~~libdbi~~      # see bellow!
    * libmaxminddb
    * libnet
    * librdkafka
    * mongo-c-driver
    * python3
    * rabbitmq-c
    * riemann-client

In the terminal all the above should look something like this

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

brew update

brew install \
    automake \
    autoconf \
    autoconf-archive \
    binutils \
    bison \
    flex \
    gcc@11 \
    glib \
    ivykis \
    json-c \
    libtool \
    pcre \
    pkg-config \
    openssl \
    #
    net-snmp \
    #
    hiredis \
    # libdbi - Do not use the homebrew provided one, see bellow!
    libmaxminddb \
    libnet \
    librdkafka \
    mongo-c-driver \
    python3 \
    rabbitmq-c \
    riemann-client
    
```

{% hint style="info" %}
* bison is required to be installed when using homebrew, because the options provided by Apple Developer Tools are incomplete. (for example: missing -W option) The reason is why bison is required to be installed from homebrew is that the -W option is supported only after 2.3.
* net-snmp might be needed as well when using homebrew, because the options provided by Apple Developer Tools are bogus a bit. The reason is why net-snmp might be required from homebrew is that the by default provided pkgconfig might give back bogus lib and include values.
* openssl - since macOS provides LibreSSL by default, we need to expand the search path of pkg-config to find the freshly installed openSSL, see bellow.
* libdbi and libdbi-drivers are [maintained and updated](modules/afsql-1.md#dependencies) in syslog-ng OSE repositories, use the latest master version from there
{% endhint %}

### Preparation

1.  Depending your macOS architecture homebrew is using different location for storing its data, so worth using a generic reference to it

    ```
    export HOMEBREW_PREFIX=$(brew --prefix)
    ```
2.  Force the building process to use bison and net-snmp installed through homebrew instead of provided by Apple Developer Tools.

    * _Option 1:_ add bison to `$PATH`

    ```
    export PATH=${HOMEBREW_PREFIX}/opt/bison/bin:${HOMEBREW_PREFIX}/opt/net-snmp/bin:${PATH}
    ```

    * _Option 2:_ when configuring set the environmental variable `$YACC` to bison

    ```
    export YACC=${HOMEBREW_PREFIX}/opt/bison
    ```
3.  Extend the search path of pkg-config to use the homebrew version of openssl and net-snmp

    ```
    export PKG_CONFIG_PATH=${HOMEBREW_PREFIX}/opt/openssl/lib/pkgconfig:${HOMEBREW_PREFIX}/opt/net-snmp/lib/pkgconfig:${PKG_CONFIG_PATH}
    ```

### Getting the source

To get the latest master from syslog-ng git you can use

```
cd YOUR_PREFERRED_WORKING_DIR   # Replace `YOUR_PREFERRED_WORKING_DIR` with your actual preferred working dir 
git clone https://github.com/syslog-ng/syslog-ng . 
```

### Configuration

```
./autogen.sh
./configure --with-ivykis=system --disable-java
```

For a full feature set (excluded the not yet supported modules on macOS):

```
./autogen.sh
./configure --with-ivykis=system --enable-all-modules --disable-smtp --disable-mqtt --disable-java --enable-tests --enable-manpages
```

{% hint style="info" %}
* for various reasons not all modules can be configured, built, and used on all macOS versions and architectures, for details please see the Modules section of this document
* [Azul](https://www.azul.com/downloads/zulu-community/) is now offering MacOS ARM builds of OpenJDK on their website in the downloads section. This has not been tested, so your mileage may vary.
{% endhint %}

### Compile and Install

```
make -j4
make install
```

Optionally, you can specify the install prefix passing `--prefix /example/installdir/` to the configure script.

{% hint style="info" %}
_F_or options and more information, read the [compile first](https://github.com/HofiOne/syslog-ng-gitbook/blob/update-macOS-compilation-instructions/chapters/chapter\_2/README.md) guide.
{% endhint %}

### Run

To start the service, you can either navigate to the syslog-ng folder where you've compiled it and run&#x20;

```
./syslog-ng -F
```

Or, you can start the service with the official command:

```
/usr/local/sbin/syslog-ng
```

{% hint style="info" %}
_F_or more information read the [run first](https://github.com/HofiOne/syslog-ng-gitbook/blob/update-macOS-compilation-instructions/chapters/chapter\_2/README.md) guide and the syslog-ng [documentation](http://www.balabit.com/sites/default/files/documents/syslog-ng-ose-latest-guides/en/syslog-ng-ose-guide-admin/html-single/index.html)
{% endhint %}

[^1]: 
